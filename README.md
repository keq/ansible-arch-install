# Ansible Arch Install
A role meant to be run independently to install my preferred Arch Linux setup.

```bash
./run.py address-of-arch-live-distro,
```

> ⚠ **WARNING**: This will wreck everything on the chosen storage device!!!
